function makeBold(text) {
    return `<b>${text}</b>`;
}

function makeItalic(text) {
    return `<i>${text}</i>`;
}

function format(formatter, text) {
    return formatter(text);
}

console.log(format(makeBold, "Foobar"));
console.log(format(makeItalic, "Foobar"));