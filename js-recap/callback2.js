const assert = require("assert");

function double(x) {
  return x * 2;
}

function halve(x) {
  return x / 2;
}

function square(x) {
  return x * x;
}

function calculate(operation, x) {
    return operation(x);
}

// Tests
assert(calculate(double, 4) === 8);
assert(calculate(halve, 4) === 2);
assert(calculate(square, 4) === 16);
console.log("All tests passed!");