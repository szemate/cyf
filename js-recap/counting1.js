const symbols = {
    "apple": "🍎",
    "banana": "🍌",
    "orange": "🍊",
};

const basket = [
    "apple", "orange", "banana", "apple", "orange", "apple",
    "apple", "orange", "banana", "orange", "apple", "apple",
    "orange", "orange", "banana", "banana", "orange", "apple",
    "apple", "orange", "orange", "apple", "banana", "apple",
    "orange", "apple", "banana", "apple", "apple", "orange",
];

const baskets = [
    [
        "apple", "orange", "banana", "apple", "orange", "apple",
        "banana", "orange", "apple", "apple", "banana", "apple",
    ],
    [
        "orange", "orange", "banana", "banana", "orange", "apple",
        "apple", "orange", "orange", "apple", "banana", "apple",
    ],
    [
        "apple", "banana", "orange", "apple", "banana", "orange",
        "orange", "apple", "banana", "apple", "apple", "orange",
    ],
    [
        "banana", "banana"
    ]
];

const basketsByCustomer = {
    "Alice": [
        "apple", "orange", "banana", "apple", "orange", "apple",
        "banana", "orange", "apple", "apple", "banana", "apple",
    ],
    "Bob": [
        "orange", "orange", "banana", "banana", "orange", "apple",
        "apple", "orange", "orange", "apple", "banana", "apple",
    ],
    "Charlie": [
        "apple", "banana", "orange", "apple", "banana", "orange",
        "orange", "apple", "banana", "apple", "apple", "orange",
    ],
};