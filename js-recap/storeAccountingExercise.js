/*
STORE ACCOUNTING
We are writing a program module for an online shop that processes purchases.
The program receives the sold items as an array of objects; each object
contains the name of the item, its price and its category (see below).
1a. Print the names and the number (the count) of all sold homeware accessories.
2a. Print the total revenue (the sum of the prices of all sold items).
3a. Print the average price of the items.
4a. Print the name of the most expensive product.

1b. i.  Write a function that takes the array of sold items as argument and returns an array of the
        names of all sold homeware accessories.
    ii. Write a function that takes the array of sold items as argument and returns the number of all
        sold homeware accessories.
2b. Write a function that takes the array of results as argument and returns the total revenue.
3b. Write a function that takes the array of sold items as argument and returns the average price of the
    items. Reuse the function that you wrote for the previous exercise.
4b. Write a function that takes the array of sold items as argument and returns the name of the
    most expensive product.
*/
const items = [
    { product: "Premier Housewives Stainless Steel Toolset", price: 19.97, category: "homeware" },
    { product: "iChinchin Women's Long Sleeve T Shirt", price: 15.99, category: "cloting" },
    { product: "U-Design Toothbrush Holder Set", price: 10.55, category: "homeware" },
    { product: "Elgar 400W Hand Mixer", price: 25.99, category: "appliances" },
    { product: "Great Gatsby White and Gold Kettle", price: 49.99, category: "homeware" },
    { product: "AM Bristol Men's Fitted Boxers", price: 24.99, category: "clothing" },
    { product: "Disnoy Brittle Small Cookie Jar", price: 10.01, category: "homeware" },
    { product: "Oreal-C Cross Action Electric Toothbrush", price: 22.19, category: "appliances" },
    { product: "Wrundole Designs Duck Wall Clock", price: 34.50, category: "homeware" },
    { product: "RuralComfort Dressing Gown for Men", price: 26.99, category: "clothing" },
  ];