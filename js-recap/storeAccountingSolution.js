/*
STORE ACCOUNTING
We are writing a program module for an online shop that processes purchases.
The program receives the sold items as an array of objects; each object
contains the name of the item, its price and its category (see below).
*/
const items = [
    { product: "Premier Housewives Stainless Steel Toolset", price: 19.97, category: "homeware" },
    { product: "iChinchin Women's Long Sleeve T Shirt", price: 15.99, category: "clothing" },
    { product: "U-Design Toothbrush Holder Set", price: 10.55, category: "homeware" },
    { product: "Elgar 400W Hand Mixer", price: 25.99, category: "appliances" },
    { product: "Great Gatsby White and Gold Kettle", price: 49.99, category: "homeware" },
    { product: "AM Bristol Men's Fitted Boxers", price: 24.99, category: "clothing" },
    { product: "Disnoy Brittle Small Cookie Jar", price: 10.01, category: "homeware" },
    { product: "Oreal-C Cross Action Electric Toothbrush", price: 22.19, category: "appliances" },
    { product: "Wrundole Designs Duck Wall Clock", price: 34.50, category: "homeware" },
    { product: "RuralComfort Dressing Gown for Men", price: 26.99, category: "clothing" },
];

// 1. Print the names and the number (the count) of all sold homeware accessories.

// Solution 1 (procedural)
const homewareProducts = [];
let homewareCount = 0;
for (const item of items) {
    if (item.category === "homeware") {
        homewareProducts.push(item.product);
        homewareCount++;
    }
}
console.log(
    `1/1 - Homeware products: ${homewareCount} (${homewareProducts.join(", ")})`
);

// Solution 2 (functional)
const homewareProducts2 = items.filter(
    item => item.category === "homeware"
).map(
    item => item.product
);
const homewareCount2 = homewareProducts2.length;
console.log(
    `1/2 - Homeware products: ${homewareCount2} (${homewareProducts2.join(", ")})`
);

// 2. Print the total revenue (the sum of the prices of all sold items).

// Solution 1 (procedural)
let revenue = 0;
for (const item of items) {
    revenue += item.price;
}
const roundedRevenue = revenue.toFixed(2);
console.log(`2/1 - Total revenue: £${roundedRevenue}`);

// Solution 2 (functional)
const revenue2 = items.reduce(
    (revenue, item) => revenue + item.price, 0
).toFixed(2);
console.log(`2/2 - Total revenue: £${revenue2}`);

// 3. Print the average price of the items. Make sure that the price is
//    expressed in pounds and pence (is rounded to 2 decimal points).

const averagePrice = (revenue / items.length).toFixed(2);
console.log(`3 - Average price: £${averagePrice}`);

// 4. Print the name of the most expensive product.

// Solution 1 (procedural)
let mostExpensiveItem = items[0];
for (let i = 1; i < items.length; i++) {
    if (items[i].price > mostExpensiveItem.price) {
        mostExpensiveItem = items[i];
    }
}
const mostExpensiveProduct = mostExpensiveItem.product;
console.log(`4/1 - The most expensive product: ${mostExpensiveProduct}`);

// Solution 2 (functional)
const mostExpensiveProduct2 = items.reduce(
    (mostExpensiveItem, item) =>
        item.price > mostExpensiveItem.price ? item : mostExpensiveItem
).product;
console.log(`4/2 - The most expensive product: ${mostExpensiveProduct2}`);