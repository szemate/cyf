/*
TEST RESULT GRADING
We are writing a program for teachers that helps them grade test results. The
program receives the test results as an array of objects; each object contains
the name of a student and their test score (see below).
*/
const results = [
    { student: "Gytha Wheatley", score: 44 },
    { student: "Alishia Thorpe", score: 83 },
    { student: "Zack Vernon", score: 59 },
    { student: "Jaclyn Elliott", score: 38 },
    { student: "Ashlynn Albinson", score: 66 },
    { student: "Slade Marchand", score: 24 },
    { student: "Karen Warwick", score: 90 },
    { student: "Michael Foster", score: 65 },
    { student: "Dove Siddall", score: 39 },
    { student: "Frederick Pond", score: 72 },
];

// 1. Print the names and the number (the count) of the students who passed the
//    test (had at least 40 points).

// Solution 1 (procedural)
const passedStudents = [];
let passedCount = 0;
for (const result of results) {
    if (result.score >= 40) {
        passedStudents.push(result.student);
        passedCount++;
    }
}
console.log(
    `1/1 - Passed students: ${passedCount} (${passedStudents.join(", ")})`
);

// Solution 2 (functional)
const passedStudents2 = results.filter(
    result => result.score >= 40
).map(
    result => result.student
);
const passedCount2 = passedStudents2.length;
console.log(
    `1/2 - Passed students: ${passedCount2} (${passedStudents2.join(", ")})`
);

// 2. Print the average score.

// Solution 1 (procedural)
let totalScore = 0;
for (const result of results) {
    totalScore += result.score;
}
const averageScore = totalScore / results.length;
console.log(`2/1 - Average score: ${averageScore}`);

// Solution 2 (functional)
const averageScore2 = results.reduce(
    (totalScore, result) => totalScore + result.score, 0
) / results.length;
console.log(`2/2 - Average score: ${averageScore2}`);

// 3. Print the name of the student who had the highest score.

// Solution 1 (procedural)
let bestResult = results[0];
for (let i = 1; i < results.length; i++) {
    if (results[i].score > bestResult.score) {
        bestResult = results[i];
    }
}
const bestStudent = bestResult.student;
console.log(`3/1 - The best student: ${bestStudent}`);

// Solution 2 (functional)
const bestStudent2 = results.reduce(
    (bestResult, result) => result.score > bestResult.score ? result : bestResult
).student;
console.log(`3/2 - The best student: ${bestStudent2}`);

// 4. Print the percentages of passes and failures (e.g. 60% passed and 40%
//    failed).

const passedPercent = Math.round(passedCount / results.length * 100);
const failedPercent = 100 - passedPercent;
console.log(`4 - Stats: ${passedPercent}% passed and ${failedPercent}% failed`);

// 5. Grade the results A-F and print a list of the students' names with their
//    grades. Use the following grading scale:
//     - 0-39: F
//     - 40-54: D
//     - 55-69: C
//     - 70-84: B
//     - 85-100: A

function getGradeForScore(score) {
    if (score < 40) {
        return "F";
    }
    if (score < 55) {
        return "D";
    }
    if (score < 70) {
        return "C";
    }
    if (score < 85) {
        return "B";
    }
    return "A";
}

// Solution 1 (procedural)
console.log("5/1 - Graded results:");
for (const result of results) {
    const grade = getGradeForScore(result.score);
    console.log(`  ${result.student}: ${grade}`);
}

// Solution 2 (functional)
console.log("5/2 - Graded results:");
results.map(result => {
    return {student: result.student, grade: getGradeForScore(result.score)};
}).forEach(
    gradedResult => console.log(`  ${gradedResult.student}: ${gradedResult.grade}`)
);