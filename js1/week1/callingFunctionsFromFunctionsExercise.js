// https://syllabus.codeyourfuture.io/js-core-1/week-1/lesson#exercise-20-mins

function getBirthYear(age) {
    const currentYear = 2021;
    return currentYear - age;
}

function getBirthYearMessage(name, age) {
    const birthYear = getBirthYear(age);
    const message = `Dear ${name}, you were born in ${birthYear}`;
    return message;
}

console.log(getBirthYearMessage("Alice", 24));
console.log(getBirthYearMessage("Bob", 42));
