// https://syllabus.codeyourfuture.io/js-core-1/week-1/lesson#exercise-5-mins

const name = "Máté";

// '+' operator
console.log("Hello " + name);

// literal string interpolation
console.log(`Hello ${name}`);
