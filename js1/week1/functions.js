const firstName1 = "Winnie";
const lastName1 = "Pooh";
const greeting1 = `Hi there, ${firstName1} ${lastName1}`;
console.log(greeting1);

const firstName2 = "Christopher";
const lastName2 = "Robin";
const greeting2 = `Hi there, ${firstName2} ${lastName2}`;
console.log(greeting2);
