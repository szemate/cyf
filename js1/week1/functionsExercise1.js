// https://gist.github.com/szemate/b56b88784047858158786229e9e6aa7d#exercise-1

// Returns a message that congratulates a member for their anniversary
function getAnniversaryGreeting(firstName, lastName, joiningYear) {
    const currentYear = 2021;
    const membershipLength = currentYear - joiningYear;
    const message = "Dear " + firstName + " " + lastName +
        ", thank you for being a member for " + membershipLength + " years!";
    return message;
}

let greeting = getAnniversaryGreeting("Jon", "Snow", 2000);
console.log(greeting);

greeting = getAnniversaryGreeting("Arya", "Stark", 2013);
console.log(greeting);

greeting = getAnniversaryGreeting("Tywin", "Lannister", 1987);
console.log(greeting);
