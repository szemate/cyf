// https://gist.github.com/szemate/b56b88784047858158786229e9e6aa7d#exercise-2

function getFixedDiscount(price) {
    const multiplier = 0.9;
    return price * multiplier;
}

function getVariableDiscount(price, discountPercent) {
    const multiplier = 1 - discountPercent / 100;
    return price * multiplier;
}

const annualFee = 100;
const monthlyFee = 10;

let reducedAnnualFee = getFixedDiscount(annualFee);
let reducedMonthlyFee = getFixedDiscount(monthlyFee);
console.log("Reduced annual fee: " + reducedAnnualFee);
console.log("Reduced monthly fee: " + reducedMonthlyFee);

reducedAnnualFee = getVariableDiscount(annualFee, 10);
reducedMonthlyFee = getVariableDiscount(monthlyFee, 5);
console.log("Reduced annual fee: " + reducedAnnualFee);
console.log("Reduced monthly fee: " + reducedMonthlyFee);
