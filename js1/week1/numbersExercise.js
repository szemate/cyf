// https://syllabus.codeyourfuture.io/js-core-1/week-1/lesson#exercise-10-mins
// https://syllabus.codeyourfuture.io/js-core-1/week-1/lesson#exercise-5-mins-1

const numberOfStudents = 15;
const numberOfMentors = 8;

const numberOfParticipants = numberOfStudents + numberOfMentors;
console.log("Total number of students and mentors: " + numberOfParticipants);

const ratioOfStudents = numberOfStudents / (numberOfStudents + numberOfMentors);
const percentOfStudents = Math.round(ratioOfStudents * 100);
console.log("Percentage of students: " + percentOfStudents);

const ratioOfMentors = numberOfMentors / (numberOfStudents + numberOfMentors);
const percentOfMentors = Math.round(ratioOfMentors * 100);
console.log("Percentage of mentors: " + percentOfMentors);
