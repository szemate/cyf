// https://syllabus.codeyourfuture.io/js-core-1/week-1/lesson#exercise-5-minutes-2

let colors = "blue, yellow";

// Solution 1
let colorsType = typeof colors;
console.log(colorsType);

// Solution 2
console.log(typeof colors);
