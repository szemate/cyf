// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise-5-mins-5
const assert = require("assert");

function secondMatchesAmy(array) {
    if (array[1] === "Amy") {
        return "Second element matched!";
    }
    return "Second element not matched";
}

// Tests
assert(secondMatchesAmy(["Rose", "Amy"]) === "Second element matched!");
assert(secondMatchesAmy(["Amy", "Rose"]) === "Second element not matched");
assert(secondMatchesAmy([]) === "Second element not matched");
