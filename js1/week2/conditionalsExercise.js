// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise-10-mins-1
const assert = require("assert");

function reportMood(mood) {
    if (typeof mood === "number") {
        return "Beep beep boop";
    } else if (mood === "happy") {
        return "Good job, you're doing great!";
    } else if (mood === "sad") {
        return "Every cloud has a silver lining";
    } else {
        return "I'm sorry, I'm still learning about feelings!";
    }
}

// Alternative solution:
// `else if` and `else` are unnecessary because we return from every branch
function reportMoodSimplified(mood) {
    if (typeof mood === "number") {
        return "Beep beep boop";
    }
    if (mood === "happy") {
        return "Good job, you're doing great!";
    }
    if (mood === "sad") {
        return "Every cloud has a silver lining";
    }
    return "I'm sorry, I'm still learning about feelings!";
}

// Tests
assert(reportMood("happy") === "Good job, you're doing great!");
assert(reportMood("sad") === "Every cloud has a silver lining");
assert(reportMood(5) === "Beep beep boop");
assert(reportMood("wacky") === "I'm sorry, I'm still learning about feelings!");
