// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise-10-minutes-1
const assert = require("assert");

function sumNumbersUntil(n) {
    let sum = 0;
    for (let i = 0; i <= n; i++) {
        sum += i;
    }
    return sum;
}

// Tests
assert(sumNumbersUntil(0) === 0);
assert(sumNumbersUntil(1) === 1);
assert(sumNumbersUntil(5) === 1 + 2 + 3 + 4 + 5);
assert(sumNumbersUntil(-5) === 0);
