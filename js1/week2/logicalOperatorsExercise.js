// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise-15-mins
const assert = require("assert");

function validateUser(userName, userType) {
    const isSuperUser = userType === "admin" || userType === "manager";
    const firstChar = userName.charAt(0);
    const length = userName.length;
    const isUserNameValid = firstChar >= "A" && firstChar <= "Z" &&
        length >= 5 && length <= 10;

    if (isSuperUser || isUserNameValid) {
        return "Username valid";
    }
    return "Username invalid";
}

// Tests
assert(validateUser("Charlie", "regular") === "Username valid"); // valid
assert(validateUser("Bob", "regular") === "Username invalid"); // too short
assert(validateUser("Cruella de Vil", "regular") === "Username invalid"); // too long
assert(validateUser("alice", "regular") === "Username invalid"); // isn't capitalised
assert(validateUser("boo", "admin") === "Username valid"); // admin user
assert(validateUser("boo", "manager") === "Username valid"); // manager user
