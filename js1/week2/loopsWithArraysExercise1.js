// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise-10-mins-2

// while loop
function logElementsWithWhileLoop(array) {
    let index = 0;
    while (index < array.length) {
        console.log(array[index]);
        index++;
    }
}

// "classic" for loop
function logElementsWithForLoop(array) {
    for (let index = 0; index < array.length; index++) {
        console.log(array[index]);
    }
}

// for...in loop
function logElementsWithForInLoop(array) {
    for (let index in array) {
        console.log(array[index]);
    }
}

// for...of loop
function logElementsWithForOfLoop(array) {
    for (let element of array) {
        console.log(element);
    }
}

const students = ["Ahmed", "Maria", "Atanas", "Nahidul", "Jack"];
logElementsWithWhileLoop(students);
logElementsWithForLoop(students);
logElementsWithForInLoop(students);
logElementsWithForOfLoop(students);
