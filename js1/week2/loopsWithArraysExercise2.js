// https://syllabus.codeyourfuture.io/js-core-1/week-2/lesson#exercise---extra-credit-20-mins
const assert = require("assert");

function square(numbers) {
    const squaredNumbers = [];
    for (let index in numbers) {
        squaredNumbers[index] = numbers[index] * numbers[index];
    }
    return squaredNumbers;
}

function filterEven(numbers) {
    // Note: the solution would be much more simple if we used
    // `evenNumbers.push` to add a new element, but it's only covered next week
    const evenNumbers = [];
    let indexOfEvenNumbers = 0;
    for (let number of numbers) {
        if (number % 2 === 0) {
            evenNumbers[indexOfEvenNumbers] = number;
            indexOfEvenNumbers++;
        }
    }
    return evenNumbers;
}

// Tests

function assertThatArraysEqual(array1, array2) {
    assert(array1.length === array2.length);
    for (let index in array1) {
        assert(array1[index] === array2[index]);
    }
}

const testArray = [-2, -1, 0, 1, 2];

const squaredArray = square(testArray);
assertThatArraysEqual(squaredArray, [4, 1, 0, 1, 4]);

const filteredArray = filterEven(testArray);
assertThatArraysEqual(filteredArray, [-2, 0, 2]);

const filteredAndSquaredArray = square(filteredArray);
assertThatArraysEqual(filteredAndSquaredArray, [4, 0, 4]);

const squaredAndFilteredArray = filterEven(squaredArray);
assertThatArraysEqual(squaredAndFilteredArray, [4, 0, 4]);
