// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-5-minutes

// Create an array with the names of the people on your table
const students = [
    "Aisha",
    "Ahmad",
    "Ali",
    "Domenico",
    "Hozan",
    "Johnny",
    "Kara",
    "Laura",
    "Negin",
    "Nigel",
    "Omar",
    "Rafael",
    "Ramla",
    "Yasemin",
    "Yunus",
];

// `console.log` out the names and how many people are at the table
console.log("List of students:", students);
console.log("Number of students:", students.length);

// Put someone from another table at the beginning of the array
students.unshift("Alice");
// Add someone else to the end of the list
students.push("Bob");
console.log("Extended list of students:", students);
