// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-10-minutes

const studentGroup1 = [
    "Aisha",
    "Johnny",
    "Ramla",
    "Laura",
    "Kara",
    "Ali",
    "Hozan",
];

const studentGroup2 = [
    "Nigel",
    "Yunus",
    "Domenico",
    "Omar",
    "Negin",
    "Ahmad",
    "Yasemin",
    "Rafael",
];

// Combine it with another array filled with the names from another table
const students = studentGroup1.concat(studentGroup2);
console.log("All students:", students);

// `console.log` the names in alphabetical order
students.sort();
console.log("Sorted students:", students);

// Create a new value with the 2nd, 3rd and 4th people in it
const smallGroupOfStudents = students.slice(2, 5);
console.log("2nd, 3rd and 4th people:", smallGroupOfStudents);

// Create a function...
function isNameInGroup(name, group) {
    if (group.includes(name)) {
        return `${name} is sitting at the table with ${group}`;
    }
    return `${name} is not sitting at the table with ${group}`;
}

console.log(isNameInGroup("Omar", studentGroup1));
console.log(isNameInGroup("Ramla", studentGroup1));
