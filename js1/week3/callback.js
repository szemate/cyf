// callbacks, anonymous functions & arrow functions

function thank(name) {
    console.log(`Thank you, ${name}!`);
}

function apologise(name) {
    console.log(`I'm sorry, ${name}!`);
}

const person = "Alice";
thank(person);
apologise(person);
