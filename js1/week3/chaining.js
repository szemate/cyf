// chaining

const names = ["daniel", "james", "irina", "mozafar", "ashleigh"];

function formatName(name) {
    return name.split("")[0].toUpperCase() + name.slice(1);
}

function log(name, index) {
    console.log(index + ": " + name);
}

const namesFormatted = names.map(formatName);
namesFormatted.forEach(log);