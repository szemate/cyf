// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-15-minutes

const messyStrings = [
    100,
    "iSMael",
    55,
    45,
    "sANyiA",
    66,
    "JaMEs",
    "eLAmIn",
    23,
    "IsMael",
    67,
    19,
    "ElaMIN",
];

// Solution 1 with named functions

function isString(value) {
    return typeof value === "string";
}

function format(string) {
    return string.toUpperCase() + "!";
}

const orderlyStrings1 = messyStrings.filter(isString).map(format);
console.log(orderlyStrings1);


// Solution 2 with anonymous functions

const orderlyStrings2 = messyStrings.filter(function(value) {
    return typeof value === "string";
}).map(function(string) {
    return string.toUpperCase() + "!";
});
console.log(orderlyStrings2);


// Solution 3 with arrow functions

const orderlyStrings3 = messyStrings
    .filter((value) => typeof value === "string")
    .map((string) => string.toUpperCase() + "!");
console.log(orderlyStrings3);
