// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-10-mins

const birthYears = [1964, 2008, 1999, 2005, 1978, 1985, 1919];

// A good practice to store all 'magic numbers' in global constants
const ageLimit = 17;
const currentYear = 2021;

function isAllowedToDrive(birthYear) {
    return birthYear <= currentYear - ageLimit;
}

const yearsAllowedToDrive = birthYears.filter(isAllowedToDrive);
console.log(
    "These are the birth years of people who can drive:",
    yearsAllowedToDrive
);


// Alternative solution with arrow function

console.log(
    "These are the birth years of people who can drive:",
    birthYears.filter((birthYear) => birthYear <= currentYear - ageLimit)
);
