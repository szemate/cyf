// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-10-mins-1

const myName = "Máté";

function isItMyName(name) {
    return name === myName;
}

function isMyNameFound(names) {
    const found = names.find(isItMyName);
    if (found !== undefined) {
        return "Found me!";
    }
    return "Haven't found me :(";
}

console.log(
    "First try:",
    isMyNameFound(["Daniel", "James", "Irina"])
);

console.log(
    "Second try:",
    isMyNameFound(["Daniel", "Máté", "Irina"])
);


// Alternative solution: 'find' is not necessary

function isMyNameFound2(names) {
    if (names.includes(myName)) {
        return "Found me!";
    }
    return "Haven't found me :(";
}
