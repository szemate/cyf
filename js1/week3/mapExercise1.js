// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-5-minutes-1
// + the exercise above it that doesn't have a link

const birthYears = [1964, 2008, 1999, 2005, 1978, 1985, 1919]; 

// A good practice to store all 'magic numbers' in global constants
const ageLimit = 17;
const currentYear = 2021;

// Exercise 1

function getAge(birthYear) {
    return currentYear - birthYear;
}

const ages = birthYears.map(getAge);
console.log(ages);

// Exercise 2

function getDriverStatus(birthYear) {
    const age = getAge(birthYear);

    if (age < ageLimit) {
        const yearsLeft = ageLimit - age;
        return `Born in ${birthYear} can drive in ${yearsLeft} years`;
    }
    return `Born in ${birthYear} can drive`;
}

const statuses = birthYears.map(getDriverStatus);
console.log(statuses);
