// https://syllabus.codeyourfuture.io/js-core-1/week-3/lesson#exercise-10-minutes-1

function abracaFunction(yourFunc) {
    console.log(
        "I am abracaFunction! Watch as I mutate an array of strings to your heart's content!"
    );
    const abracaArray = [
        "James",
        "Elamin",
        "Ismael",
        "Sanyia",
        "Chris",
        "Antigoni",
    ];

    const abracaOutput = yourFunc(abracaArray);

    return abracaOutput;
}

// Solution 1 with named functions

function makeOneWordUpperCase(word) {
    return word.toUpperCase();
}

function makeAllWordsUpperCase(words) {
    return words.map(makeOneWordUpperCase);
}

const upperCasedNames1 = abracaFunction(makeAllWordsUpperCase);
console.log(upperCasedNames1);


// Solution 2 with anonymous functions

const upperCasedNames2 = abracaFunction(function(words) {
    return words.map(function(word) {
        return word.toUpperCase();
    });
});
console.log(upperCasedNames2);


// Solution 3 with arrow functions

const upperCasedNames3 = abracaFunction((words) => words.map((word) => word.toUpperCase()));
console.log(upperCasedNames3);
