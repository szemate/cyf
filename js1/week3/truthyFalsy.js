// truthy & falsy values

const condition = true;

if (condition) {
    console.log("It's true!");
} else {
    console.log("It's false!");
}
