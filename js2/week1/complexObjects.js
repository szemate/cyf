let laptop = {
    brand: "Lenovo",
    screenSize: 13,
    isWorking: true,
    isUnderRepair: false,
    getStatus: function() {
        if (this.isWorking) {
            return "Working correctly";
        } else if (this.isUnderRepair) {
            return "Under repair";
        } else {
            return "Waiting for repair";
        }
    },
    sendForRepair: function() {
        if (!this.isWorking) {
            this.isUnderRepair = true;
        }
    },
};

let workspace = {
    desk: {
        material: "wood",
        isStanding: false,
    },
    stationaries: ["pen", "notebook"],
    laptop: laptop,
};

console.log(workspace);
console.log(workspace.laptop.getStatus());

workspace.laptop.isWorking = false;
console.log(workspace.laptop.getStatus());

workspace.laptop.sendForRepair();
console.log(workspace.laptop.getStatus());
