let house = {
  address: "1 Kinning Park",
  previousOwners: ["Claire M.", "John A."],
  currentOwner: {
    firstName: "Margaret",
    lastName: "Conway",
  },
};

let newCurrentOwner = {
  firstName: "Georgina",
  lastName: "Hernandez",
};

house.address = "51 Berkley Road";
house.previousOwners = ["Brian M.", "Fiona S."];
house.currentOwner.lastName = "Montgomery";

house.currentOwner = newCurrentOwner;
house.previousOwners[1] = "Stephen B.";
house.isForSale = false;

console.log(house);

/* * * * */

let kinningParkHouse = {
  address: "1 Kinning Park",
  price: 180000,
  currentOwner: {
    firstName: "Margaret",
    lastName: "Conway",
    email: "margaret@fake-emails.com",
  },
};

let parkAvenueHouse = {
  address: "50 Park Avenue",
  price: 195000,
  currentOwner: {
    firstName: "Marie",
    lastName: "McDonald",
    email: "marie.m@real-emails.com",
  },
};

function getOwnerFullName(house) {
    return house.currentOwner.firstName + " " + house.currentOwner.lastName;
}

function getEmailAddresses(house1, house2) {
    return [house1.currentOwner.email, house2.currentOwner.email];
}

function getCheapestAddress(house1, house2) {
    if (house1.price < house2.price) {
        return house1.address;
    }
    return house2.address;
}

console.log(getOwnerFullName(kinningParkHouse));
console.log(getEmailAddresses(kinningParkHouse, parkAvenueHouse));
console.log(getCheapestAddress(kinningParkHouse, parkAvenueHouse));
