let person1 = {
    name: "Alice",
    age: 25,
    greet: function() {
        return "Hello!";
    },
    sayName: function() {
        return this.name;
    },
};

console.log(person1.greet());
console.log(person1.sayName());

/* * * * */

let person2 = {
    name: "Alice",
    friends: ["John", "Nina"],
    makeFriend: function(newFriend) {
        this.friends.push(newFriend);
    },
};

person2.makeFriend("Bob");
console.log(person2.friends);

/* * * * */

let coffeeMachine = {
    brand: "Super Coffee",
    prices: {
        cappuccino: 2.4,
        blackCoffee: 1.5,
        flatWhite: 3.0,
    },
    insertedAmount: 0,
    insertMoney: function (amount) {
        this.insertedAmount += amount;
    },
    getCoffee: function (coffee) {
        if (this.insertedAmount >= this.prices[coffee]) {
            this.insertedAmount -= this.prices[coffee];
            return `Please take your ${coffee}`;
        }
        return `Sorry you don't have enough money for a ${coffee}`;
    },
};

coffeeMachine.insertMoney(2.4);
console.log(coffeeMachine.getCoffee("cappuccino"));

coffeeMachine.insertMoney(1.5);
console.log(coffeeMachine.getCoffee("blackCoffee"));

coffeeMachine.insertMoney(3.0);
console.log(coffeeMachine.getCoffee("flatWhite"));

coffeeMachine.insertMoney(2.4);
console.log(coffeeMachine.getCoffee("flatWhite"));
