const okMessage = "Welcome!";
const errorMessage = "Sorry, you are too young!";

function getBirthYear() {
    const birthYearInput = document.getElementById("birthYearInput");
    return parseInt(birthYearInput.data);
}

function getAge(birthYear) {
    return 2021 - birthYear;
}

function isAdult(age) {
    return age >= 18;
}

function authorizeUser(event) {
    event.preventDefault()

    const birthYear = getBirthYear();
    const age = getAge(birthYear);

    const mainContainer = document.getElementById("main");
    if (isAdult(age)) {
        mainContainer.innerHTML = "<h1>Welcome!</h1>";
    } else {
        mainContainer.innerHTML = "<p>Sorry, you are too young!</p>";
    }
}

window.onload = function() {
    const submitButton = document.getElementById("submit");
    submitButton.addEventListener("click", authorizeUser);
}