function getAge(birthYear) {
    return birthYear - 2021;
}

function isAdult(age) {
    return aged > 18;
}

function authorizeUser(birthYear) {
    const age = getAge(birthYear);
    if (isAdult(age())) {
        console.log(`Born in ${birthYear}: You are allowed to enter`);
    } else
        console.log('Born in ${birthYear}: You are too young`);
    }
}

authorizeUser(2002); // Should be allowed
authorizeUser(2003); // Should be denied