const fetch = require("node-fetch");

function fetchSuccessfully() {
    fetch("https://meowfacts.herokuapp.com/")
        .then(function(response) {
            return response.json();
        })
        .then(function(data) {
            console.log(data);
        })
        .catch(function(error) {
            console.log("An error occurred:", error);
        });
}

function fetchWithError() {
    fetch("https://httpstat.us/404")
        .then(function(response) {
            if (response.ok) {
                return response.json();
            }
            throw `${response.status} ${response.statusText}`;
        })
        .then(function(data) {
            console.log(data);
        })
        .catch(function(error) {
            console.log("An error occurred:", error);
        });
}

async function fetchWithAwait() {
    try {
        const response = await fetch("https://meowfacts.herokuapp.com/");
        const data = await response.json();
        console.log(data);
    } catch (error) {
        console.log("An error occurred:", error);
    }
}

fetchSuccessfully();
// fetchWithError();
// fetchWithAwait();
