const employeeObj = {
    name: "Alice",
    age: 27,
    position: "developer",
    startDate: "01-01-2021",
    endDate: null,
    manager: undefined,
    devices: [
        "laptop",
        "monitor",
    ],
};
console.log("Original employee object:");
console.log(employeeObj);

const employeeJSON = JSON.stringify(employeeObj, null, 2);
console.log("Employee JSON:");
console.log(employeeJSON);

// const decodedEmployeeObj = JSON.parse(employeeJSON);
// console.log("Decoded employee object:");
// console.log(decodedEmployeeObj);
