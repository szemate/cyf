const util = require("util");
const sleep = util.promisify(setTimeout);
/* Ignore the stuff above */

/* Things to demonstrate:
    1. Promises are objects
    2. Resolving a promise
    3. Rejecting a promise (-> exception.js)
    4. Returning a value from 'then'
    5. Returning a promise from 'then'
*/

const promise1 = sleep(2000);
console.log(promise1);

promise1.then(function() {
    console.log("And miles to go before I sleep");
});

console.log("But I have promises to keep");