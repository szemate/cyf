const buyPotatoes, chop, fry, serve;


function makeChipsSynchronously() {
    // In this example the `buyPotatoes`, `chop` and `fry` functions block the
    // event loop until the chips are served, that is, we cannot execute any
    // other piece of code while we are waiting for `buyPotatoes`, `chop` and
    // `fry` to complete.
    const potatoes = buyPotatoes();
    const choppedPotatoes = chop(potatoes);
    const chips = fry(choppedPotatoes);
    serve(chips);
}


function makeChipsWithCallbacks() {
    // In this example the `buyPotatoes`, `chop` and `fry` functions run
    // asynchronously, that is, they don't block the event loop; but don't
    // return anything.
    buyPotatoes(function(potatoes) {
        // `potatoes` is a value that `buyPotatoes` passes into the callback
        // function.
        chop(potatoes, function(choppedPotatoes) {
            // `choppedPotatoes` is a value that `chop` passes into the
            // callback function.
            fry(choppedPotatoes, function(chips) {
                // `chips` is a value that `fry` passes into the callback
                // function.
                serve(chips);
            });
        });
    });
}


function makeChipsWithPromises() {
    // In this example the `buyPotatoes`, `chop` and `fry` functions run
    // asynchronously, that is, they don't block the event loop; and they
    // return promises.
    const promiseToBuy = buyPotatoes();
    // At this point `buyPotatoes` is already getting executed, but we haven't
    // specified what to do when it's completed.

    // The `promiseToBuy.then` method returns the promise that the `chop`
    // function returns.
    const promiseToChop = promiseToBuy.then(function(potatoes) {
        // `potatoes` is the value that `promiseToBuy` gets resolved with.
        return chop(potatoes);
    });

    // The `promiseToChop.then` method returns the promise that the `fry`
    // function returns.
    const promiseToFry = promiseToChop.then(function(choppedPotatoes) {
        // `choppedPotatos` is the value that `promiseToChop` gets resolved
        // with.
        return fry(choppedPotatoes);
    });

    promiseToFry.then(function(chips) {
        // `chips` is the value that `promiseToFry` gets resolved with.
        serve(chips);
    });
}


function makeChipsWithPromiseChaining() {
    // This example is identical to the `makeChipsWithPromises` function above
    // but we use anonymous promises, we don't assign them to variables.
    buyPotatoes().then(function(potatoes) {
        // This is the callback of the `then` method of the 1st promise.
        return chop(potatoes);
    }).then(function(choppedPotatoes) {
        // This is the callback of the `then` method of the 2nd promise.
        return fry(choppedPotatoes);
    }).then(function(chips) {
        // This is the callback of the `then` method of the 3rd promise.
        serve(chips);
    });
}


async function makeChipsWithPromiseAwait() {
    // This example is very similar to the `makeChipsSynchronously` function,
    // but because it's asynchronous, it doesn't block the event loop, that is,
    // we can execute other pieces of code while we are waiting for
    // `buyPotatoes`, `chop` and `fry` to complete.

    // The return value of `buyPotatoes` is a promise, but the return value of
    // `await buyPotatoes` is the value that the promise gets resolved with.
    const potatoes = await buyPotatoes();

    // The return value of `chop` is a promise, but the return value of
    // `await chop` is the value that the promise gets resolved with.
    const choppedPotatoes = await chop(potatoes);

    // The return value of `fry` is a promise, but the return value of
    // `await fry` is the value that the promise gets resolved with.
    const chips = await fry(choppedPotatoes);

    serve(chips);
}
