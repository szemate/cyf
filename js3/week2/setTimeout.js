function runSynchronously() {
    console.log("when I do");
    console.log("count the clock");
    console.log("that tells the time");
}

function runAsynchronously() {
    setTimeout(function() {
        console.log("when I do");
    }, 2000);
    setTimeout(function() {
        console.log("count the clock");
    }, 1000);
    console.log("that tells the time");
}

runSynchronously();
// runAsynchronously();
