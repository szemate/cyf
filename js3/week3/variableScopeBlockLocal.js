function demonstrateBlockLocalScope() {
    // Outer block
    if (true) {
        // Inner block
        let blockLocalVar = "I'm here!"
        console.log("Inner block:", blockLocalVar);
    }
    console.log("Outer block:", blockLocalVar);
}

demonstrateBlockLocalScope();
console.log("Main block:", blockLocalVar);
