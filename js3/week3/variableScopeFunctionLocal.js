function demonstrateFunctionLocalScope() {
    // Outer block
    if (true) {
        // Inner block
        var functionLocalVar = "I'm here!";
        console.log("Inner block:", functionLocalVar);
    }
    console.log("Outer block:", functionLocalVar);
}

demonstrateFunctionLocalScope();
console.log("Main block:", functionLocalVar);
