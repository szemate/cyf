function demonstrateGlobalScope() {
    // Outer block
    if (true) {
        // Inner block
        globalVar = "I'm here!";
        console.log("Inner block:", globalVar);
    }
    console.log("Outer block:", globalVar);
}

demonstrateGlobalScope();
console.log("Main block:", globalVar);
