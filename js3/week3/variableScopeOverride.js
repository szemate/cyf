let blockLocalVar = "I'm in the main block!"

function demonstrateBlockLocalScope() {
    // Outer block
    let blockLocalVar = "I'm in the outer block!"

    if (true) {
        // Inner block
        let blockLocalVar = "I'm in the inner block!"
        console.log("Inner block:", blockLocalVar);
    }

    console.log("Outer block:", blockLocalVar);
}

demonstrateBlockLocalScope();
console.log("Main block:", blockLocalVar);
