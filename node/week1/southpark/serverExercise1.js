const {getStudents} = require("./students");
const express = require("express");

const students = getStudents();
const app = express();

app.listen(3000, function () {
    console.log("Server is listening on port 3000.");
});
