const {getStudents} = require("./students");
const express = require("express");

const students = getStudents();
const app = express();

app.get("/profiles/eric", function (req, res) {
    const student = students.find(student => student.username === "eric");
    res.json(student);
});

app.get("/profiles/eric/image", function (req, res) {
    if (!req.query.size || req.query.size === "large") {
        res.sendFile(`${__dirname}/images/eric-large.jpg`);
    } else if (req.query.size === "small") {
        res.sendFile(`${__dirname}/images/eric-small.jpg`);
    } else {
        res.sendStatus(400);
    }
});

app.listen(3000, function () {
    console.log("Server is listening on port 3000.");
});
