const {getStudents} = require("./students");
const express = require("express");

const students = getStudents();
const app = express();

app.get("/profiles", function (req, res) {
    const extendedStudents = students.map(student => {
        student.profile = `/profiles/${student.username}`;
        student.largeImage = `/profiles/${student.username}/image?size=large`;
        student.smallImage = `/profiles/${student.username}/image?size=small`;
        return student;
    });
    res.json(extendedStudents);
});

app.get("/profiles/:username", function (req, res) {
    const student = students.find(
        student => student.username === req.params.username);

    if (student === undefined) {
        res.sendStatus(404);
    } else {
        res.json(student);
    }
});

app.get("/profiles/:username/image", function (req, res) {
    const username = req.params.username;

    if (!req.query.size || req.query.size === "large") {
        res.sendFile(`${__dirname}/images/${username}-large.jpg`);
    } else if (req.query.size === "small") {
        res.sendFile(`${__dirname}/images/${username}-small.jpg`);
    } else {
        res.sendStatus(400);
    }
});

app.listen(3000, function () {
    console.log("Server is listening on port 3000.");
});
