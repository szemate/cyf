const express = require('express');

const app = express();

app.use(express.json());

let albumsData = [
    {
        albumId: "10",
        artistName: "Beyoncé",
        collectionName: "Lemonade",
        artworkUrl100: "http://is1.mzstatic.com/image/thumb/Music20/v4/23/c1/9e/23c19e53-783f-ae47-7212-03cc9998bd84/source/100x100bb.jpg",
        releaseDate: "2016-04-25T07:00:00Z",
        primaryGenreName: "Pop",
        url: "https://www.youtube.com/embed/PeonBmeFR8o?rel=0&amp;controls=0&amp;showinfo=0",
    },
    {
        albumId: "11",
        artistName: "Beyoncé",
        collectionName: "Dangerously In Love",
        artworkUrl100: "http://is1.mzstatic.com/image/thumb/Music/v4/18/93/6d/18936d85-8f6b-7597-87ef-62c4c5211298/source/100x100bb.jpg",
        releaseDate: "2003-06-24T07:00:00Z",
        primaryGenreName: "Pop",
        url: "https://www.youtube.com/embed/ViwtNLUqkMY?rel=0&amp;controls=0&amp;showinfo=0",
    },
];

app.get("/albums", (req, res) => {
    res.send(albumsData);
});

app.get("/albums/:id", (req, res) => {
    const album = albumsData.find(album => album.albumId === req.params.id);

    if (album) {
        res.send(album);
    } else {
        res.status(404); // Not found
        res.send({"message": "no such album"});
    }
});

app.post("/albums", (req, res) => {
    const newAlbum = req.body;

    // Data validation
    if (!newAlbum.albumId) {
        res.status(400); // Bad request
        res.send({"message": "album ID is missing"});
    } else if (albumsData.find(album => album.albumId === newAlbum.albumId)) {
        res.status(400); // Bad request
        res.send({"message": "an album with this ID already exists"});
    } else {
        albumsData.push(newAlbum);
        res.status(201); // Created
        res.send(newAlbum);
    }
});

app.delete("/albums/:id", (req, res) => {
    // Solution 1
    const albumIndex = albumsData
        .findIndex(album => album.albumId === req.params.id);
    if (albumIndex >= 0) {
        albumsData.splice(albumIndex, 1);
    }

    // Solution 2
    albumsData = albumsData.filter(album => album.albumId !== req.params.id);

    res.status(204); // No data
    res.end(); // Response body is empty
});

app.put("/albums/:id", (req, res) => {
    const albumIndex = albumsData
        .findIndex(album => album.albumId === req.params.id);

    if (albumIndex >= 0) {
        const origAlbum = albumsData[albumIndex];
        const updatedAlbum = req.body;

        // Data validation
        if (updatedAlbum.albumId &&
                origAlbum.albumId !== updatedAlbum.albumId) {
            res.status(400); // Bad request
            res.send({"message": "album IDs don't match"});
        } else {
            albumsData[albumIndex] = {...origAlbum, ...updatedAlbum};
            res.send(albumsData[albumIndex]);
        }
    } else {
        res.status(404); // Not found
        res.send({"message": "no such album"});
    }
});

app.listen(3000, () => console.log("Running on port 3000"));
