const users = [
    {
        id: 0,
        email: "alice@mail.com",
        firstName: "Alice",
        lastName: "Evans",
        isAdmin: true,
    },
    {
        id: 1,
        email: "bob@mail.com",
        firstName: "Bob",
        lastName: "Thornton",
        isAdmin: false,
    },
];
