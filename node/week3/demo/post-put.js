const express = require('express');

const app = express();

// app.use(express.json());

const profiles = [];

app.post('/profile', (req, res) => {
    console.log(req.body);
    res.send("OK");
});

app.put('/profile/:username', (req, res) => {
    console.log(req.body);
    res.send("OK");
});

app.listen(3000, () => console.log("Running on port 3000"));
