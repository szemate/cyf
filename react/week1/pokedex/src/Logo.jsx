const Logo = (props) => {
    return (
        <header>
            <h1>{props.appName}</h1>
            <img src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/016.png" />
        </header>
    );
};

export default Logo;
