import BestPokemon from "./BestPokemon";
import CaughtPokemon from "./CaughtPokemon";
import Logo from "./Logo";

const App = () => {
    const abilities = ['Anticipation', 'Adaptability', 'Run-Away'];
    const date = new Date().toLocaleDateString();

    const logWhenClicked = () => {
        console.log("The logo has been clicked");
    };

    return (
        <div>
            <Logo appName="Pokedex" handleClick={logWhenClicked} />
            <BestPokemon abilities={abilities} />
            <CaughtPokemon date={date} />
        </div>
    );
};

export default App;
