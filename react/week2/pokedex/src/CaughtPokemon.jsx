import { useState } from "react";

const CaughtPokemon = (props) => {
    const [numCaught, setNumCaught] = useState(0);

    const catchPokemon = () => {
        setState((num) => num + 1);
    };

    return (
        <div>
            <p>Caught {numCaught} Pokemon on {props.date}</p>
            <button onClick={catchPokemon}>Catch Pokemon</button>
        </div>
    );
};

export default CaughtPokemon;
