import Counter from "./Counter";
import Lister from "./Lister";

const App = () => {
    return (
        <div id="App">
            <Counter index="1" />
            <hr />
            <Counter index="2" />
            <hr />
            <Lister />
        </div>
    );
};

export default App;
