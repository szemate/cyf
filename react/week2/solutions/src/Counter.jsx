import { useState } from "react";

const Counter = (props) => {
    const [count, setCount] = useState(0);
    console.log("Rendering counter", props.index, "for count", count);

    const incrementCount = () => {
        setCount((prevCount) => prevCount + 1);
    };

    return (
        <div>
            <h1>Counter {props.index}</h1>
            <p>You clicked {count} times.</p>
            <button onClick={incrementCount}>Click me!</button>
        </div>
    );
};

export default Counter;
