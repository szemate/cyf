import { useState } from "react";

const Lister = () => {
    const [state, setState] = useState({
        items: [],
        newItem: "",
    });
    console.log("Rendering Lister for state", state);

    const onInput = (event) => {
        // Only update the 'newItem' property and copy the rest of the
        // 'prevState' object into the new object with '...prevState'
        setState((prevState) => ({
            ...prevState,
            newItem: event.target.value,
        }));
    };

    const onSubmit = (event) => {
        // Prevent submitting the form because it would reload the page
        event.preventDefault();

        setState((prevState) => {
            // Don't update the state if the input field is empty
            if (prevState.newItem.trim() === "") {
                return prevState;
            }

            // Get the current value of 'newItem' from 'prevState' to make sure
            // it's always the latest value ('state.newItem' might be stale)
            return {
                items: prevState.items.concat(prevState.newItem),
                newItem: "",
            };
        });
    };

    return (
        <div>
            <h1>Lister</h1>
            <ul>
                {state.items.map((item, index) => (
                    <li key={`item${index}`}>{item}</li>
                ))}
            </ul>
            <form>
                <input
                    type="text"
                    value={state.newItem}
                    onChange={onInput}
                />
                <button onClick={onSubmit}>Add item</button>
            </form>
        </div>
    );
};

export default Lister;
